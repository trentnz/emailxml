﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmailChecker.Models
{
	public class Expense
	{
		public string CostCentre { get; set; }
		public string Total { get; set; }
		public string PaymentMethod { get; set; }
	}
}