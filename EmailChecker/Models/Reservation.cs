﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmailChecker.Models
{
	public class Reservation
	{
		public string Vendor { get; set; }
		public string Description { get; set; }
		public string Date { get; set; }
		public Expense Expense { get; set; }
	}
}