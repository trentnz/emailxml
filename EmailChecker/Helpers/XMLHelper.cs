﻿using EmailChecker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace EmailChecker
{
	public class XMLHelper
	{
		public const string UNKNOW_COST_CENTRE = "UNKNOWN";
		public string RawData { get; set; }
		public string CleanedData { get; set; }
		public XDocument ParsedDocument { get; set; }
		public XMLHelper(string rawData)
		{
			this.RawData = rawData;
		}

		public void ExtractXML()
		{
			List<string> matches = new List<string>();
			Regex regex = new Regex("<([^<>]+).*>.*</\\1>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline);
			Match match = regex.Match(this.RawData);
			while (match.Success)
			{
				matches.Add(match.Value);
				match = match.NextMatch();
			}
			this.CleanedData = "<data>" + String.Join(String.Empty, matches.ToArray()) + "</data>";

		}

		public bool XMLIsValid()
		{
			bool isValid = false;

			try
			{
				if (this.CleanedData + "" != "")
				{
					this.ParsedDocument = XDocument.Parse(this.CleanedData);

					string total = GetElementValueByName("total");

					if (total != "")
					{
						isValid = true;
					}
				}
			}
			catch (Exception ex)
			{
				isValid = false;
			}
			return isValid;
		}

		private string GetElementValueByName(string elementName) {
			XElement element  = ParsedDocument.Descendants().Where(n => n.Name.LocalName == elementName).FirstOrDefault();
			return element == null ? "" : element.Value;
		}

		public Reservation Save()
		{
			Reservation reservation = new Reservation();
			reservation.Date = GetElementValueByName("date");
			reservation.Vendor = GetElementValueByName("vendor");
			reservation.Description = GetElementValueByName("description");
			reservation.Expense = new Expense();
			reservation.Expense.CostCentre = GetElementValueByName("cost_centre");
			reservation.Expense.Total = GetElementValueByName("total");
			reservation.Expense.PaymentMethod = GetElementValueByName("payment_method");
			if (reservation.Expense.CostCentre + "" == "") { reservation.Expense.CostCentre = UNKNOW_COST_CENTRE; };
			return reservation;

		}
	}
}