﻿using EmailChecker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EmailChecker.Controllers
{

	class ReservationStore
	{
		public static List<Reservation> Reservations;
		public ReservationStore() {

		}
	}
	

	public class ValuesController : ApiController
	{

		// GET api/values
		public List<Reservation> Get()
		{
			return ReservationStore.Reservations;
		}


		// POST api/values
		public HttpResponseMessage Post()
		{
			XMLHelper xmlHelper = new XMLHelper(Request.Content.ReadAsStringAsync().Result);
			xmlHelper.ExtractXML();
			if (xmlHelper.XMLIsValid())
			{
				if (ReservationStore.Reservations == null)
				{
					ReservationStore.Reservations = new List<Reservation>();
				}

				ReservationStore.Reservations.Add(xmlHelper.Save());
				return new HttpResponseMessage(HttpStatusCode.OK);
			} else
			{
				return new HttpResponseMessage(HttpStatusCode.BadRequest);
			}
			
		}

	}
}
